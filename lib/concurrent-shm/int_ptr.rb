module ConcurrentSHM
  # Value pointers.
  module Value
    # Base class for integer pointers.
    # @abstract Subclasses must override {#read} and {#write}.
    class IntPtr
      # Read from the pointer.
      # @return [Integer]
      def read
        raise NotImplementedError
      end

      # Write to the pointer.
      # @param value [Integer]
      # @return [nil]
      def write(value)
        raise NotImplementedError
      end

      # Read from the pointer. If `bit` is non-nil, read the specified bit.
      # @param bit [Integer] the bit position to read
      # @return [Integer] the value of the pointer
      # @raise [ArgumentError] if the bit is not nil or an integer
      def [](bit=nil)
        return read if bit.nil?
        return (read >> bit) & 0x1 if bit.is_a?(Integer)

        raise ArgumentError, "Invalid index: #{bit.inspect}"
      end

      # Writes to the pointer. If `bit` is non-nil, write the specified bit.
      # @param bit [Integer] the bit position to read
      # @param v [Integer] the value to write
      # @raise [IntDomainError] if the write under or overflows
      # @raise [ArgumentError] if the value is not an integer or boolean, or if the bit is not nil or an integer
      def []=(bit=nil, v)
        case bit
        when nil
          write(v)
        when Integer
          if v
            write(read | (1 << bit))
          else
            write(read & ~(1 << bit))
          end

        else
          raise ArgumentError, "Invalid index: #{bit.inspect}"
        end
      end
    end
  end
end
