module ConcurrentSHM
  # A channel for asynchronously passing data between processes, safely, via
  # shared memory spaces.
  class Channel
    # Raised when `#send` is called on a closed channel.
    class ClosedWriteError
      def initialize
        super("Write to closed channel")
      end
    end

    # An unbuffered channel.
    class Unbuffered < Channel
      # An unbuffered channel with zero-width packets.
      class Empty < Unbuffered
        # Allocates an unbuffered, zero-width channel.
        # @see Channel.new
        def self.new(shm, offset: 0, autosize: true)
          alloc(shm, 2, offset: offset, autosize: autosize) do |body|
            @state = body[0].as_intptr
            @closed = body[1].as_intptr
          end
        end

        # Send a zero-width packet over the channel. Blocks if there are no receivers.
        # @return [nil]
        # @raise [ClosedWriteError] if the channel is closed
        def send
          do_send
        end

        # Receive a zero-width packet over the channel. Blocks if there are no senders.
        # @return [nil]
        def recv
          do_recv
        end
      end

      # An unbuffered channel with fixed-width packets.
      class Fixed < Unbuffered
        # Allocates an unbuffered, fixed-width channel with the specified width.
        # @see Channel.new
        # @raise [RangeError] if the width is less than 1
        def self.new(shm, width:, offset: 0, autosize: true)
          raise RangeError, "Width must be > 0" unless width > 0

          alloc(shm, 2 + width, offset: offset, autosize: autosize) do |body|
            @state = body[0].as_intptr
            @closed = body[1].as_intptr
            @data = body[2..]
          end
        end

        # Send a fixed-width packet over the channel. Blocks if there are no receivers.
        # @param data [String] the packet
        # @return [nil]
        # @raise [ClosedWriteError] if the channel is closed
        # @raise [RangeError] if the packet size exceeds the channel width
        def send(data)
          raise RangeError, "Data is wider than channel" if data.size > @data.size

          do_send do
            @data.write(data)
          end
        end

        # Receive a fixed-width packet over the channel. Blocks if there are no senders.
        # @return [String] the packet
        def recv
          do_recv do
            @data.read
          end
        end
      end

      # An unbuffered channel with variable-width packets.
      class Variable < Unbuffered
        # Allocates an unbuffered, variable-width channel with the specified
        # capacity.
        # @see Channel.new
        # @raise [RangeError] if the capacity is less than 1 or greater than 2<sup>32</sup>-1
        def self.new(shm, capacity:, offset: 0, autosize: true)
          raise RangeError, "Capacity must be > 0" unless capacity > 0
          cbytes = bytes_for(capacity) { raise RangeError, "Capacity must be less than 2^32" }

          alloc(shm, cbytes + 2 + capacity, offset: offset, autosize: autosize) do |body|
            @width = body[0, cbytes].as_uintptr
            @state = body[cbytes].as_intptr
            @closed = body[cbytes+1].as_intptr
            @data = body[cbytes+2..]
          end
        end

        # Send a variable-width packet over the channel. Blocks if there are no receivers.
        # @param data [String] the packet
        # @return [nil]
        # @raise [ClosedWriteError] if the channel is closed
        # @raise [RangeError] if the packet size exceeds the channel capacity
        def send(data)
          raise RangeError, "Data is wider than channel" if data.size > @data.size

          do_send do
            @width[] = data.bytesize + 1
            @data.write(data)
          end
        end

        # Receive a variable-width packet over the channel. Blocks if there are no senders.
        # @return [String] the packet
        def recv
          do_recv do
            w = @width[] - 1
            @width[] = 0
            @data[0...w].read
          end
        end
      end

      private

      EMPTY =  0
      FULL  = +1
      DONE  = -1
      private_constant :EMPTY, :FULL, :DONE

      def do_send
        locked do
          raise ClosedWriteError unless transition(@state, from: EMPTY, to: FULL)
          yield if block_given?

          raise ClosedWriteError unless transition(@state, from: DONE, to: EMPTY)
          return nil
        end
      end

      def do_recv
        locked do
          return :closed unless transition(@state, from: FULL, to: DONE)
          yield if block_given?
        end
      end
    end

    # A buffered channel with a buffer depth of 1.
    class SingleBuffered < Channel
      # A single-buffered channel with zero-width packets.
      class Empty < SingleBuffered
        # Allocates a single-buffered, zero-width channel.
        # @see Channel.new
        def self.new(shm, offset: 0, autosize: true)
          alloc(shm, 2, offset: offset, autosize: autosize) do |body|
            @state = body[0].as_intptr
            @closed = body[1].as_intptr
          end
        end

        # (see Buffered::Empty#send)
        def send
          do_send
        end

        # (see Buffered::Empty#recv)
        def recv
          do_recv
        end
      end

      # A single-buffered channel with fixed-width packets.
      class Fixed < SingleBuffered
        # Allocates a single-unbuffered, fixed-width channel with the specified
        # width.
        # @see Channel.new
        # @raise [RangeError] if width is less than 1
        def self.new(shm, width:, offset: 0, autosize: true)
          raise RangeError, "Width must be > 0" unless width > 0

          alloc(shm, 2 + width, offset: offset, autosize: autosize) do |body|
            @state = body[0].as_intptr
            @closed = body[1].as_intptr
            @data = body[2..]
          end
        end

        # (see Buffered::Fixed#send)
        def send(data)
          raise RangeError, "Data is wider than channel" if data.size > @data.size

          do_send do
            @data.write(data)
          end
        end

        # (see Buffered::Fixed#recv)
        def recv
          do_recv do
            @data.read
          end
        end
      end

      # A single-buffered channel with variable-width packets.
      class Variable < SingleBuffered
        # Allocates a single-buffered, variable-width channel with the specified
        # capacity.
        # @see Channel.new
        # @raise [RangeError] if capacity is less than 1 or greater than 2<sup>32</sup>-1
        def self.new(shm, capacity:, offset: 0, autosize: true)
          raise RangeError, "Capacity must be > 0" unless capacity > 0
          cbytes = bytes_for(capacity) { raise RangeError, "Capacity must be less than 2^32" }

          alloc(shm, cbytes + 1 + capacity, offset: offset, autosize: autosize) do |body|
            @width = body[0, cbytes].as_uintptr
            @closed = body[cbytes].as_intptr
            @data = body[cbytes+1..]
          end
        end

        # Send a variable-width packet over the channel. Blocks if the buffer is full.
        # @param data [String] the packet
        # @return [nil]
        # @raise [ClosedWriteError] if the channel is closed
        # @raise [RangeError] if the packet size exceeds the channel capacity
        def send(data)
          raise RangeError, "Data is wider than channel" if data.size > @data.size

          do_send do
            @width[] = data.bytesize
            @data.write(data)
          end
        end

        # Receive a variable-width packet over the channel. Blocks if the buffer is empty.
        # @return [String] the packet
        def recv
          do_recv do
            @data[0...@width[]].read
          end
        end
      end

      private

      EMPTY = 0
      FULL  = 1
      private_constant :EMPTY, :FULL

      def do_send
        locked do
          raise ClosedWriteError unless transition(@state, from: EMPTY, to: FULL)
          yield if block_given?
          return nil
        end
      end

      def do_recv
        locked do
          return :closed unless transition(@state, from: FULL, to: EMPTY)
          yield if block_given?
        end
      end
    end

    # A buffered channel.
    class Buffered < Channel
      # A buffered channel with zero-width packets.
      class Empty < Buffered
        # Allocates a buffered, zero-width channel.
        # @see Channel.new
        # @raise [RangeError] if depth is less than 1 or greater than 2<sup>32</sup>-1
        def self.new(shm, depth:, offset: 0, autosize: true)
          raise RangeError, "Depth must be > 0" unless depth > 0
          dbytes = bytes_for(depth) { raise RangeError, "Depth must be less than 2^32" }

          alloc(shm, dbytes*2 + 1, offset: offset, autosize: autosize) do |body|
            @depth = depth
            @read = body[0, dbytes].as_uintptr
            @written = body[dbytes, dbytes].as_uintptr
            @closed = body[dbytes*2].as_intptr
          end
        end

        # Send a zero-width packet over the channel. Blocks if the buffer is full.
        # @return [nil]
        # @raise [ClosedWriteError] if the channel is closed
        def send
          do_send
        end

        # Receive a zero-width packet over the channel. Blocks if the buffer is empty.
        # @return [nil]
        def recv
          do_recv
        end
      end

      # A buffered channel with fixed-width packets.
      class Fixed < Buffered
        # Allocates a buffered, fixed-width channel with the specified width.
        # @see Channel.new
        # @raise [RangeError] if width is less than 1 or depth is less than 1 or greater than 2<sup>32</sup>-1
        def self.new(shm, depth:, width:, offset: 0, autosize: true)
          raise RangeError, "Width must be > 0" unless width > 0
          raise RangeError, "Depth must be > 0" unless depth > 0
          dbytes = bytes_for(depth) { raise RangeError, "Depth must be less than 2^32" }

          alloc(shm, dbytes*2 + 1 + depth*width, offset: offset, autosize: autosize) do |body|
            @depth, @width = depth,
            @read = body[0, dbytes].as_uintptr
            @written = body[dbytes, dbytes].as_uintptr
            @closed = body[dbytes*2].as_intptr

            data = body[dbytes*2+1..]
            @data = (0...@depth).map { |i| data[i*width, width] }
          end
        end

        # Send a fixed-width packet over the channel. Blocks if the buffer is full.
        # @param data [String] the packet
        # @return [nil]
        # @raise [ClosedWriteError] if the channel is closed
        # @raise [RangeError] if the packet size exceeds the channel width
        def send(data)
          raise RangeError, "Data is wider than channel" if data.size > @data.size

          do_send do |i|
            @data[i].write(data)
          end
        end

        # Receive a fixed-width packet over the channel. Blocks if the buffer is empty.
        # @return [String] the packet
        def recv
          do_recv do |i|
            @data[i].read
          end
        end
      end

      private

      def do_send
        locked do
          raise ClosedWriteError unless wait_until { @written[] < @depth }

          i = @written[]
          w = (i + 1) % @depth
          @written[] = w == @read[] ? @depth : w
          @cond.signal
          yield(i) if block_given?
          nil
        end
      end

      def do_recv
        locked do
          return :closed unless wait_until { @read[] != @written[] }

          r = @read[]
          @read[] = (r + 1) % @depth
          @written[] = r if @written[] == @depth
          @cond.signal
          yield(r) if block_given?
        end
      end
    end

    class << self
      # (see .alloc)
      # Allocates a channel in a shared memory space at the specified offset,
      # optionally initializing the space to the required size. The class of the
      # returned channel depends on the width and depth.
      #
      # | Depth | Width | Behavior
      # |-|-|-
      # | 0 | 0 | {Unbuffered::Empty Unbuffered, zero-width}
      # | 0 | Positive | {Unbuffered::Fixed Unbuffered, fixed-width}
      # | 0 | Negative | {Unbuffered::Variable Unbuffered, variable-width}
      # | 1 | 0 | {SingleBuffered::Empty Single-buffered, zero-width}
      # | 1 | Positive | {SingleBuffered::Fixed Single-buffered, fixed-width}
      # | 1 | Negative | {SingleBuffered::Variable Single-buffered, variable-width}
      # | 2+ | 0 | {Buffered::Empty Buffered, zero-width}
      # | 2+ | Positive | {Buffered::Fixed Buffered, fixed-width}
      # | 2+ | Negative | Not supported (buffered, variable-width)
      #
      # When allocating a variable-width channel, `width` is inverted and passed
      # as `capacity`.
      #
      # @param shm [SharedMemory] the shared memory space
      # @param offset [Integer] the offset to place the channel at in the shared memory space
      # @param autosize [Boolean] whether to initialize the space to the required size
      # @param depth [Integer] the channel buffer depth
      # @param width [Integer] the channel data width
      # @return [Channel] the channel
      # @raise [ArgumentError] if depth is not an integer, width is not an integer, or depth is negative
      # @raise [RangeError] if the offset is not a multiple of 16, or the shared memory space is not large enough
      def new(shm, depth:, width:, offset: 0, autosize: true)
        raise ArgumentError, "Depth is not an integer" unless depth.is_a?(Integer)
        raise ArgumentError, "Width is not an integer" unless width.is_a?(Integer)
        raise ArgumentError, "Depth must be positive" if depth < 0

        args = { offset: offset, autosize: autosize }

        if depth == 0
          if width < 0
            Unbuffered::Variable.new(shm, capacity: -width, **args)
          elsif width > 0
            Unbuffered::Fixed.new(shm, width: width, **args)
          else
            Unbuffered::Empty.new(shm, **args)
          end

        elsif depth == 1
          if width < 0
            SingleBuffered::Variable.new(shm, capacity: -width, **args)
          elsif width > 0
            SingleBuffered::Fixed.new(shm, width: width, **args)
          else
            SingleBuffered::Empty.new(shm, **args)
          end

        else
          if width < 0
            raise "Buffered variable-width mode is not supported"
          elsif width > 0
            Buffered::Fixed.new(shm, depth: depth, width: width, **args)
          else
            Buffered::Empty.new(shm, depth: depth, **args)
          end
        end
      end

      private

      def mu_size
        ConcurrentSHM.size_of(Mutex)
      end

      def cond_size
        ConcurrentSHM.size_of(Condition)
      end

      def alloc(shm, needed, offset:, autosize:, &block)
        raise RangeError, "Offset must be aligned to a 16-byte boundary" if offset % 16 != 0

        needed += mu_size + cond_size
        shm.size = offset + needed if autosize && shm.size == 0
        raise RangeError, "Shared memory is too small: size=#{shm.size} but channel needs #{needed} at offset #{offset}" if shm.size < offset + needed

        r = Region.map(shm, offset, needed)
        mu = Mutex.new(r[0, mu_size])
        cond = Condition.new(r[mu_size, cond_size])
        body = r[mu_size + cond_size..]

        # Call Object.new, because Channel.new has been overwritten
        ch = Channel.superclass.method(:new).unbind.bind(self).call
        ch.instance_variable_set(:@mu, mu)
        ch.instance_variable_set(:@cond, cond)

        # Always retain a reference to the mapped region so it doesn't get garbage collected
        ch.instance_variable_set(:@mapping, r)

        # Implementation-specific initialization
        ch.instance_exec(body, &block)

        # Clear status
        ch.instance_variable_get(:@closed)[] = 0

        ch
      end

      def bytes_for(v)
        if v < 1 << 8
          1
        elsif v < 1 << 16
          2
        elsif v < 1 << 32
          4
        else
          yield
        end
      end
    end

    # Close the channel.
    # @return [nil]
    # @raise [RuntimeError] if the channel is already closed
    def close
      locked do
        raise "Already closed" if closed?

        @cond.broadcast
        @closed[] = 1
      end
    end

    private

    def closed?
      @closed[] == 1
    end

    def locked
      @mu.lock
      yield
    ensure
      @mu.unlock
    end

    def wait_until
      loop do
        return true if yield
        return false if closed?
        @cond.wait(@mu)
      end
    end

    def transition(value, from:, to:)
      return false unless wait_until { value[] == from }
      value[] = to
      @cond.signal
      true
    end
  end
end
