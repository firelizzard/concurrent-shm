# Shared Memory Concurrency
module ConcurrentSHM
  # Gem data
  module Gem
    # Gem name
    NAME = 'concurrent-shm'

    # Gem version
    VERSION = '0.1.0'

    # Gem summary
    SUMMARY = 'Shared Memory Concurrency'

    # Gem description
    DESCRIPTION = 'Multi-process concurrent structures utilizing shared memory - NOT SUPPORTED FOR WINDOWS'

    # Gem authors
    AUTHORS = ['Ethan Reesor']

    # Gem website
    WEBSITE = 'https://gitlab.com/firelizzard/concurrent-shm'

    # Gem license
    LICENSE = 'Apache-2.0'
  end
end
