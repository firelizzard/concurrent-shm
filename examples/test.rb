require 'concurrent-shm'

# `stty -icanon -echo`
# at_exit { `stty icanon echo` }

Signal.trap('INT', 'EXIT')

begin
  shm = ConcurrentSHM::SharedMemory.open("/foo-bar", 0600, mode: :anon)
rescue Errno::EEXIST
  ConcurrentSHM::SharedMemory.delete("/foo-bar")
  retry
end

ch = ConcurrentSHM::Channel::Buffered::Fixed.new(shm, width: 1, depth: 5)

pid = Process.pid
Thread.new do
  if pid != Process.pid
    Thread.new do
      sleep 1 while Process.kill(0, Process.ppid)
    rescue Errno::EPERM
      # ignore
    ensure
      exit
    end
  end

  i = 0
  loop do
    c = ch.recv
    STDERR.puts "received! #{c.inspect}"
    sleep 0.1
  end

  puts "closed!"
end

STDIN.each_line do |l|
  l.each_char do |c|
    ch.send c
    puts "sent! #{c.inspect}"
  end
end
