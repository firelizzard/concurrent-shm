require File.join(__dir__, 'lib', 'concurrent-shm', 'gem.rb')

Gem::Specification.new do |s|
  s.name        = ConcurrentSHM::Gem::NAME
  s.version     = ConcurrentSHM::Gem::VERSION
  s.summary     = ConcurrentSHM::Gem::SUMMARY
  s.description = ConcurrentSHM::Gem::DESCRIPTION
  s.authors     = ConcurrentSHM::Gem::AUTHORS
  s.homepage    = ConcurrentSHM::Gem::WEBSITE
  s.license     = ConcurrentSHM::Gem::LICENSE
  s.files       = Dir['lib/**/*.rb', 'ext/**/*.[ch]'] + %w(LICENSE AUTHORS README.md CHANGELOG.md)
  s.extensions  = Dir['ext/**/extconf.rb']
end
