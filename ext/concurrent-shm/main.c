#include "main.h"

void Init_PtrTypes(VALUE ns, VALUE base);
void Init_posix(VALUE ConcurrentSHM);

void Init_cshm()
{
    VALUE ConcurrentSHM = rb_define_module("ConcurrentSHM");

    Init_posix(ConcurrentSHM);

    VALUE Value = rb_define_module_under(ConcurrentSHM, "Value");
    VALUE IntPtr = rb_define_class_under(Value, "IntPtr", rb_cObject);
    Init_PtrTypes(Value, IntPtr);
}
