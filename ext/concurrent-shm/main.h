#include <ruby.h>

/* To prevent unused parameter warnings */
#define UNUSED(x) (void)(x)

// __shared_retain -> increments and returns new value
// __shared_release -> decrements and returns new value

#ifdef __APPLE__
#include <Availability.h>
#if defined(__MAC_10_12) && __MAC_OS_X_VERSION_MAX_ALLOWED >= __MAC_10_12
#include <stdatomic.h>
#define __shared_retain(ptr) (atomic_fetch_add_explicit(ptr, 1, memory_order_acquire) + 1)
#define __shared_release(ptr) (atomic_fetch_sub_explicit(ptr, 1, memory_order_release) - 1)
typedef atomic_uint_fast32_t refcount;
#else
#include <libkern/OSAtomic.h>
#define __shared_retain(ptr) OSAtomicIncrement32Barrier(ptr)
#define __shared_release(ptr) OSAtomicDecrement32Barrier(ptr)
typedef uint32_t refcount;
#endif
#else
#define __shared_retain(ptr) __sync_add_and_fetch(ptr, 1)
#define __shared_release(ptr) __sync_sub_and_fetch(ptr, 1)
typedef uint32_t refcount;
#endif

#define debug(n, fmt, ...) \
    do { \
        VALUE args[] = {rb_str_new_cstr(fmt), ##__VA_ARGS__}; \
        rb_io_write(rb_stdout, rb_f_sprintf(n+1, args)); \
    } while(0);

#define rb_syserr_fail_strf(err, format, ...) rb_syserr_fail_str(err, rb_sprintf(format, ##__VA_ARGS__))
#define rb_check_syserr_fail_strf(r, err, format, ...) \
    do { \
        if (r) { \
            rb_syserr_fail_str(err, rb_sprintf(format, ##__VA_ARGS__)); \
        } \
    } while (0)

#define chk_err(fn, args, format, ...) \
    do { \
        int err = fn args; \
        rb_check_syserr_fail_strf(err, err, #fn "(" format ")", ##__VA_ARGS__); \
    } while (0)

#define chk_errno(fn, args, format, ...) \
    rb_check_syserr_fail_strf(fn args, errno, #fn "(" format ")", ##__VA_ARGS__)

#define PtrTypes \
    X(Int8,   int8,   __value2int,  8); \
    X(Int16,  int16,  __value2int,  16); \
    X(Int32,  int32,  __value2int,  32); \
    X(Int64,  int64,  __value2int,  64); \
    X(UInt8,  uint8,  __value2uint, 8); \
    X(UInt16, uint16, __value2uint, 16); \
    X(UInt32, uint32, __value2uint, 32); \
    X(UInt64, uint64, __value2uint, 64);

#define Ptr2ValueFn(rtype, ctype) VALUE rtype##Ptr2Value(ctype##_t * v)
#define Value2PtrFn(rtype, ctype) ctype##_t * Value2##rtype##Ptr(VALUE v)

#define X(r, c, ...) Ptr2ValueFn(r, c);
PtrTypes
#undef X

#define X(r, c, ...) Value2PtrFn(r, c);
PtrTypes
#undef X

void Init_PtrTypes(VALUE ns, VALUE base);
