#include <errno.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

#include "main.h"
#include <ruby/thread.h>
#include "varargs.h"

static VALUE Finalizer, SharedMemory, Region, Mutex, Condition;

/*
 * Document-class: ConcurrentSHM::SharedMemory
 * A POSIX shared memory object.
 */

/* Delete a POSIX shared memory object.
 * @param name [String] the name of the object
 * @return [nil]
 * @raise [ArgumentError] if the name is nil or is not a string
 * @raise [Errno::EACCES] if permission to delete the object is denied
 * @raise [Errno::ENOENT] if the named object does not exist
 */
static VALUE shared_memory_delete(VALUE self, VALUE name)
{
    UNUSED(self);

    if (NIL_P(name)) {
        rb_raise(rb_eArgError, "Name must not be nil");
    }

    char * cname = StringValueCStr(name);
    chk_errno(shm_unlink, (cname), "%s", cname);

    return Qnil;
}

/* @overload open(name, fmode, mode: :excl)
 *   Create or open a POSIX shared memory object. The default flags are
 *   `O_CREATE`, `O_RDWR`, and `O_TRUNC`.
 *
 *   If the mode is `:excl`, `:exclusive`, `:anon`, or `:anonymous`, the object
 *   is opened exclusively, with the `O_EXCL` flag in addition to the defaults.
 *
 *   If the mode is `:anon` or `:anonymous`, the object is deleted immediately
 *   after being opened. Opened and deleted shared memory objects can be used
 *   until the object is closed.
 *
 *   If the mode is `:shared`, the object is opened with the default flags.
 *
 *   @param name [String] the name of the object
 *   @param fmode [Integer] the file mode of the object
 *   @param mode [Symbol] the manner in which to create or open the object.
 *   @return [SharedMemory] the shared memory object
 *   @raise [ArgumentError] if the name is nil or is not a string
 *   @raise [Errno::EACCES] if permission to create or open the object is denied
 *   @raise [Errno::EEXIST] if `exclusive` is `true` and the object specified by `name` already exists
 *   @raise [Errno::EINVAL] if the name is invalid
 *   @raise [Errno::EMFILE] if the per-process limit on the number of open file descriptors has been reached
 *   @raise [Errno::ENAMETOOLONG] if the name is too long
 *   @raise [Errno::ENFILE] if the system-wide limit on the total number of open files has been reached
 */
static VALUE shared_memory_open(int argc, VALUE * argv, VALUE self)
{
    CONST_IDS(id_, exclusive, anonymous, shared, excl, anon);

    VALUE name, fmode;
    KWARGS(ARGS("2:", name, fmode), 0, mode);

    if (NIL_P(name)) {
        rb_raise(rb_eArgError, "Name must not be nil");
    }

    ID id_mode = mode == Qundef ? id_excl : SYM2ID(mode);
    char * cname = StringValueCStr(name);
    int oflag = O_CREAT | O_RDWR | O_TRUNC | (id_mode == id_shared ? 0 : O_EXCL);
    int fd = shm_open(cname, oflag, FIX2INT(fmode));
    if (fd < 0) {
        int err = errno;
        if (err != EMFILE && err != ENFILE && err != ENOMEM) {
            rb_syserr_fail_strf(err, "shm_open(%s, %d, %d)", cname, oflag, FIX2INT(fmode));
        }

        // if ERRNO indicates no memory, GC and try again
        rb_gc();
        fd = shm_open(cname, oflag, FIX2INT(fmode));
        rb_check_syserr_fail_strf(fd < 0, errno, "shm_open(%s, %d, %d)", cname, oflag, FIX2INT(fmode));
    }

    if (id_mode == id_anon || id_mode == id_anonymous) {
        shared_memory_delete(self, name);
    }

    return rb_funcall(self, rb_intern("new"), 2, INT2FIX(fd), name);
}

/* Create a {SharedMemory} instance for the specified file descriptor.
 * @param fd [Integer] the file descriptor
 * @param name [String] the name
 */
static VALUE shared_memory_initialize(VALUE self, VALUE fd, VALUE name)
{
    rb_call_super(1, &fd); // super(fd)
    rb_iv_set(self, "@name", name); // @name = name
    return self;
}

/* The size of the shared memory space using `fstat`.
 * @return [Integer]
 * @raise [Errno::EBADF] if the receivers `fileno` is not a valid file descriptor
 */
static VALUE shared_memory_size(VALUE self)
{
    struct stat stat;
    VALUE fd = rb_funcall(self, rb_intern("fileno"), 0);
    chk_err(fstat, (FIX2INT(fd), &stat), "fd=%d", FIX2INT(fd));
    return INT2FIX(stat.st_size);
}

/* Set the size of the shared memory space using `ftruncate`.
 * @param size [Integer] the new size of the space
 */
static VALUE shared_memory_size_eq(VALUE self, VALUE size)
{
    VALUE fd = rb_funcall(self, rb_intern("fileno"), 0);
    chk_errno(ftruncate, (FIX2INT(fd), FIX2INT(size)), "%d, %d", FIX2INT(fd), FIX2INT(size));
    return size;
}

typedef struct region {
    void * data;
    size_t len;
    char mapped;
    VALUE from;
} region_t;

struct mutex_shared {
    pthread_mutexattr_t attr;
    pthread_mutex_t mu;
    refcount refcount;
};

typedef struct mutex {
    VALUE region;
    struct mutex_shared * shared;
} mutex_t;

struct condition_shared {
    pthread_condattr_t attr;
    pthread_cond_t cond;
    refcount refcount;
};

typedef struct condition {
    VALUE region;
    struct condition_shared * shared;
} condition_t;

typedef struct finalizer {
    VALUE class;
    union {
        void * raw;
        region_t * region;
        mutex_t * mutex;
        condition_t * condition;
    } as;
} finalizer_t;

static void __region_mark(region_t * r)
{
    rb_gc_mark(r->from);
}

static void __mutex_mark(mutex_t * mu)
{
    rb_gc_mark(mu->region);
}

static void __condition_mark(condition_t * cond)
{
    rb_gc_mark(cond->region);
}

static void __finalizer_mark(finalizer_t * fin)
{
    rb_gc_mark(fin->class);
}

#define StructTypes \
    X(region); \
    X(mutex); \
    X(condition); \
    X(finalizer);

#define X(type, ...) static int __##type##_size(const type##_t * x) { return sizeof(type##_t); }
StructTypes
#undef X

#define X(type, ...) \
    static const rb_data_type_t __##type##_rb_type = { \
        .wrap_struct_name = #type "_t", \
        .function = { \
            .dmark = (void (*)(void*))__##type##_mark, \
            .dfree = RUBY_TYPED_DEFAULT_FREE, \
            .dsize = (size_t (*)(const void*))__##type##_size, \
        }, \
    };
StructTypes
#undef X

#define X(type, ...) \
    static inline type##_t * value_as_##type(VALUE self) { \
        type##_t * x; \
        TypedData_Get_Struct(self, type##_t, &__##type##_rb_type, x); \
        return x; \
    }
StructTypes
#undef X

#define X(type, ...) \
    static VALUE __##type##_alloc(VALUE self) \
    { \
        type##_t * x; \
        VALUE obj = TypedData_Make_Struct(self, type##_t, &__##type##_rb_type, x); \
        return obj; \
    }
StructTypes
#undef X

/*
 * Document-class: ConcurrentSHM::Finalizer
 * @!visibility private
 */

static void __set_finalizer(VALUE obj, void * data)
{
    VALUE fin = __finalizer_alloc(Finalizer);
    finalizer_t * f = value_as_finalizer(fin);
    f->class = rb_class_of(obj);
    f->as.raw = data;

    rb_define_finalizer(obj, fin);
}

/*
 * @!visibility private
 */
static VALUE __finalize(VALUE blk, VALUE objid)
{
    finalizer_t * f = (finalizer_t *)rb_data_object_get(blk);

    if (f->class == Region) {
        if (f->as.region->mapped && f->as.region->data) {
            if (munmap(f->as.region->data, f->as.region->len)) {
                perror("munmap");
            }
            f->as.region->data = NULL;
        }

    } else if (f->class == Mutex) {
        if (f->as.mutex->shared && __shared_release(&f->as.mutex->shared->refcount) == 0) {
            if (pthread_mutexattr_destroy(&f->as.mutex->shared->attr)) {
                perror("pthread_mutexattr_destroy");
            }
            if (pthread_mutex_destroy(&f->as.mutex->shared->mu)) {
                perror("pthread_mutex_destroy");
            }
            f->as.mutex->shared = NULL;
        }

    } else if (f->class == Condition) {
        if (f->as.condition->shared && __shared_release(&f->as.condition->shared->refcount) == 0) {
            if (pthread_condattr_destroy(&f->as.condition->shared->attr)) {
                perror("pthread_condattr_destroy");
            }
            if (pthread_cond_destroy(&f->as.condition->shared->cond)) {
                perror("pthread_cond_destroy");
            }
            f->as.condition->shared = NULL;
        }
    }

    return Qnil;
}

/*
 * Document-class: ConcurrentSHM::Region
 * A memory-mapped region of a file or shared memory space.
 */

static VALUE __new_region(void * data, size_t len, char mapped, VALUE from)
{
    VALUE self = __region_alloc(Region);
    region_t * r = value_as_region(self);
    r->data = data;
    r->len = len;
    r->mapped = mapped;
    r->from = from;

    if (mapped) {
        __set_finalizer(self, r);
    }
    return self;
}

static VALUE __region_from(void * data, size_t len, VALUE from)
{
    return __new_region(data, len, 0, from);
}

/* Memory-map a region of a file or shared memory space. The region is
 * read/write and shared.
 * @param file [IO|SharedMemory] the file or shared memory space
 * @param off [Integer] the offset of the region
 * @param len [Integer] the length of the region
 * @return [Region] the mapped region
 * @raise [Errno::EACCES] if the file is not a regular file, or the file is not
 *   open for reading, not open for writing, or append-only
 * @raise [Errno::EAGAIN] if the file is locked or too much memory has been locked
 * @raise [Errno::EBADF] if `file.lineno` is not a valid file descriptor
 * @raise [Errno::EINVAL] if the length or offset are too large or the length is 0
 * @raise [Errno::ENFILE] if the system-wide limit on the total number of open files has been reached
 * @raise [Errno::ENODEV] if the underlying filesystem of the specified file does not support memory mapping
 * @raise [Errno::ENOMEM] if no memory is available, or the process's number of mappings limit has been reached
 * @raise [Errno::EPERM] if the operation was prevented by a file seal
 */
static VALUE region_map(VALUE self, VALUE file, VALUE off, VALUE len)
{
    UNUSED(self);

    int fd = FIX2INT(rb_funcall(file, rb_intern("fileno"), 0));
    void * data = mmap(NULL, FIX2INT(len), PROT_READ | PROT_WRITE, MAP_SHARED, fd, FIX2INT(off));
    if (data == MAP_FAILED) {
        rb_syserr_fail_strf(errno, "mmap(fd=%d, len=%d)", fd, FIX2INT(len));
    }

    return __new_region(data, FIX2INT(len), 1, file);
}

/* Unmaps a memory-mapped region.
 * @param region [Region] the region to unmap
 */
static VALUE region_unmap(VALUE self, VALUE region)
{
    UNUSED(self);

    region_t * r = value_as_region(region);
    if (!r->mapped) {
        rb_raise(rb_eArgError, "Not a memory mapped region");
    }

    if (!r->data) {
        return Qnil;
    }

    chk_errno(munmap, (r->data, r->len), "");

    r->data = NULL;
    r->len = 0;
    return Qnil;
}

static VALUE __region_at_len(VALUE self, off_t off, size_t len)
{
    region_t * r = value_as_region(self);
    if (off < 0) {
        rb_raise(rb_eRangeError, "Out of range: offset is negative");
    } else if (len < 0) {
        rb_raise(rb_eRangeError, "Out of range: length is negative");
    } else if (off + len > r->len) {
        rb_raise(rb_eRangeError, "Out of range: offset %llu and length %llu exceeds region length %llu", (unsigned long long)off, (unsigned long long)len, (unsigned long long)r->len);
    }

    return __region_from((void *)((char *)r->data + off), len, self);
}

static VALUE __region_at(VALUE self, off_t begin, off_t end, VALUE exclusive)
{
    return __region_at_len(self, begin, end-begin + (exclusive == Qtrue ? 0 : 1));
}

/* Get a subregion of the receiver.
 * @return [Region] the subregion
 * @raise ArgumentError if the arguments are not one of the allowed overloads
 * @raise RangeError if the arguments do not refer to a valid subrange
 *
 * @overload [](start)
 *   The subregion from the start index to the end of the region.
 *   @param start [Integer] the starting index of the subset
 *   @raise ArgumentError unless `start` is an integer
 * @overload [](start...)
 *   The subregion from the start index to the end of the region.
 *   @param start [Integer] the starting index of the subset
 *   @raise ArgumentError unless `start` is an integer
 * @overload [](start, length)
 *   The subregion from the start index with the specified length.
 *   @param start [Integer] the starting index of the subset
 *   @param length [Integer] the length of the subset
 *   @raise ArgumentError unless `start` and `length` are integers
 * @overload [](start..end)
 *   The subregion from the start index to the end index, inclusive.
 *   @param start [Integer] the starting index of the subset
 *   @param end [Integer] the ending index of the subset
 *   @raise ArgumentError unless `start` and `end` are integers
 * @overload [](start...end)
 *   The subregion from the start index to the end index, exclusive.
 *   @param start [Integer] the starting index of the subset
 *   @param end [Integer] the ending index of the subset
 *   @raise ArgumentError unless `start` and `end` are integers
 */
static VALUE region_at(int argc, VALUE * argv, VALUE self)
{
    VALUE at, len;
    rb_scan_args(argc, argv, "11", &at, &len);

    if (RB_TYPE_P(at, T_FIXNUM) && RB_TYPE_P(len, T_FIXNUM)) {
        return __region_at_len(self, FIX2INT(at), FIX2INT(len));

    } else if (RB_TYPE_P(at, T_FIXNUM) && NIL_P(len)) {
        return __region_at_len(self, FIX2INT(at), 1);

    } else if (rb_obj_is_kind_of(at, rb_cRange) && NIL_P(len)) {
        // continue

    } else {
        rb_raise(rb_eArgError, "Region#[at, len] accepts [int,int], [int], [int...nil], or [int...int]");
    }

    VALUE begin = rb_funcall(at, rb_intern("begin"), 0);
    VALUE end = rb_funcall(at, rb_intern("end"), 0);
    VALUE excl = rb_funcall(at, rb_intern("exclude_end?"), 0);

    if (!RB_TYPE_P(begin, T_FIXNUM)) {
        rb_raise(rb_eArgError, "Region#[at, len] accepts [int,int], [int], [int...nil], or [int...int]");
    }

    if (NIL_P(end)) {
        region_t * r = value_as_region(self);
        return __region_at(self, FIX2INT(begin), r->len, Qtrue);
    }

    if (!RB_TYPE_P(end, T_FIXNUM)) {
        rb_raise(rb_eArgError, "Region#[at, len] accepts [int,int], [int], [int...nil], or [int...int]");
    }

    return __region_at(self, FIX2INT(begin), FIX2INT(end), excl);
}

/* Returns a pointer to the region as a {Value::IntPtr signed integer pointer}
 * of the same width as the region.
 * @return [Value::IntPtr] the interger pointer
 * @raise [RangeError] if the region is not 1, 2, 4, or 8 bytes wide
 */
static VALUE region_as_intptr(VALUE self)
{
    region_t * r = value_as_region(self);
    switch (r->len) {
    case 1: return Int8Ptr2Value ((int8_t *) r->data);
    case 2: return Int16Ptr2Value((int16_t *)r->data);
    case 4: return Int32Ptr2Value((int32_t *)r->data);
    case 8: return Int64Ptr2Value((int64_t *)r->data);
    default: rb_raise(rb_eRangeError, "Region length must be 1, 2, 4, or 8 bytes to use as an integer pointer");
    }
}

/* Returns a pointer to the region as an {Value::IntPtr unsigned integer pointer}
 * of the same width as the region.
 * @return [Value::IntPtr] the interger pointer
 * @raise [RangeError] if the region is not 1, 2, 4, or 8 bytes wide
 */
static VALUE region_as_uintptr(VALUE self)
{
    region_t * r = value_as_region(self);
    switch (r->len) {
    case 1: return UInt8Ptr2Value ((uint8_t *) r->data);
    case 2: return UInt16Ptr2Value((uint16_t *)r->data);
    case 4: return UInt32Ptr2Value((uint32_t *)r->data);
    case 8: return UInt64Ptr2Value((uint64_t *)r->data);
    default: rb_raise(rb_eRangeError, "Region length must be 1, 2, 4, or 8 bytes to use as an integer pointer");
    }
}

#define ralign(r, b) ((uintptr_t)r->data % b)

/* The alignment of the region to the specified byte boundary. Equivalent to
 * `addr % bytes`.
 * @return [Integer] the region's alignment to the specified boundary
 */
static VALUE region_alignment(VALUE self, VALUE bytes)
{
    region_t * r = value_as_region(self);
    return INT2FIX(ralign(r, FIX2INT(bytes)));
}

/* Returns true if the region is aligned to the specified byte boundary.
 * Equivalent to `addr % bytes == 0`.
 * @return [Boolean] `true` if the region is aligned to the specified boundary
 */
static VALUE region_aligned_q(VALUE self, VALUE bytes)
{
    region_t * r = value_as_region(self);
    return ralign(r, FIX2INT(bytes)) == 0 ? Qtrue : Qfalse;
}

/* The file, shared memory space, or region the receiver was created from.
 * @return [IO|SharedMemory|Region] the file, shared memory space, or region
 */
static VALUE region_from(VALUE self)
{
    region_t * r = value_as_region(self);
    return r->from;
}

/* The width of the region.
 * @return [Integer] the width of the region
 */
static VALUE region_size(VALUE self)
{
    region_t * r = value_as_region(self);
    return INT2FIX(r->len);
}

/* Read data from the region.
 * @return [String] the data read
 */
static VALUE region_read(VALUE self)
{
    region_t * r = value_as_region(self);
    return rb_str_new((const char *)r->data, r->len);
}

/* Write data to the region.
 * @param str [String] the data to write
 * @return [nil]
 */
static VALUE region_write(VALUE self, VALUE str)
{
    if (!RB_TYPE_P(str, T_STRING)) {
        str = rb_funcall(str, rb_intern("to_s"), 0);
        if (!RB_TYPE_P(str, T_STRING)) {
            rb_raise(rb_eArgError, "str.to_s is not a string");
        }
    }

    region_t * r = value_as_region(self);
    size_t len = RSTRING_LEN(str);
    if (len > r->len) {
        rb_raise(rb_eRangeError, "string is longer than region");
    }

    memcpy(r->data, StringValuePtr(str), len);
    return Qnil;
}

#define set_region(var, alignment, region) \
    do { \
        region_t * r = value_as_region(region); \
        if (ralign(r, alignment) != 0) { \
            rb_raise(rb_eRangeError, "region must be aligned to a %d-byte boundary", alignment); \
        } \
        if (sizeof(typeof(*var->shared)) > r->len) { \
            rb_raise(rb_eRangeError, "region too small: expected >=%lu, got %lu", sizeof(typeof(*var->shared)), r->len); \
        } \
        var->region = region; \
        var->shared = (typeof(var->shared))r->data; \
    } while(0);

/*
 * Document-class: ConcurrentSHM::Mutex
 * A POSIX-threads mutex, with the `pshared` attribute set to
 * `PTHREAD_PROCESS_SHARED`, allocated in a shared memory space.
 */

/* Initializes a shared mutex in the specified memory region.
 * @param region [Region] the memory region
 * @raise [RangeError] if the region is too small or not aligned to a 16-byte boundary
 */
static VALUE mutex_initialize(VALUE self, VALUE region)
{
    mutex_t * mu = value_as_mutex(self);
    set_region(mu, 16, region);
    __set_finalizer(self, mu);

    if (__shared_retain(&mu->shared->refcount) == 1) {
        chk_err(pthread_mutexattr_init, (&mu->shared->attr), "");
        chk_err(pthread_mutexattr_setpshared, (&mu->shared->attr, PTHREAD_PROCESS_SHARED), "");
        chk_err(pthread_mutex_init, (&mu->shared->mu, &mu->shared->attr), "");
    }
    return self;
}

/* Attempts to lock the mutex.
 * @return [Boolean] `true` if the lock was acquired
 * @raise [Errno::EAGAIN] if the mutex could not be acquired because the maximum number of recursive locks for mutex has been exceeded
 */
static VALUE mutex_try_lock(VALUE self)
{
    mutex_t * mu = value_as_mutex(self);
    int err = pthread_mutex_trylock(&mu->shared->mu);
    switch (err) {
    case 0:
        return Qtrue;
    case EBUSY:
        return Qfalse;
    default:
        rb_syserr_fail_strf(err, "mutex_try_lock");
    }
}

/* Locks the mutex, blocking until the mutex becomes available, if necessary.
 * @return [nil]
 * @raise [Errno::EDEADLK] if the current thread already owns the mutex
 * @raise [Errno::EAGAIN] if the mutex could not be acquired because the maximum number of recursive locks for mutex has been exceeded
 */
static VALUE mutex_lock(VALUE self)
{
    if (mutex_try_lock(self) == Qtrue) {
        return Qnil;
    }

    mutex_t * mu = value_as_mutex(self);
    int err = (int)(uintptr_t)rb_thread_call_without_gvl((void * (*)(void *))pthread_mutex_lock, &mu->shared->mu, NULL, NULL);

    rb_check_syserr_fail_strf(err, err, "pthread_mutex_lock()");
    return Qnil;
}

/* Unlocks the mutex.
 * @raise [Errno::EPERM] if the current thread does not own the mutex
 * @raise [Errno::EAGAIN] if the mutex could not be acquired because the maximum number of recursive locks for mutex has been exceeded
 */
static VALUE mutex_unlock(VALUE self)
{
    mutex_t * mu = value_as_mutex(self);
    chk_err(pthread_mutex_unlock, (&mu->shared->mu), "");
    return Qnil;
}

/*
 * Document-class: ConcurrentSHM::Condition
 * A POSIX-threads condition variable, with the `pshared` attribute set to
 * `PTHREAD_PROCESS_SHARED`, allocated in a shared memory space.
 */

/* Initializes a shared condition variable in the specified memory region.
 * @param region [Region] the memory region
 * @raise [RangeError] if the region is too small or not aligned to a 16-byte boundary
 */
static VALUE condition_initialize(VALUE self, VALUE region)
{
    condition_t * cond = value_as_condition(self);
    set_region(cond, 16, region);
    __set_finalizer(self, cond);

    if (__shared_retain(&cond->shared->refcount) == 1) {
        chk_err(pthread_condattr_init, (&cond->shared->attr), "");
        chk_err(pthread_condattr_setpshared, (&cond->shared->attr, PTHREAD_PROCESS_SHARED), "");
        chk_err(pthread_cond_init, (&cond->shared->cond, &cond->shared->attr), "");
    }
    return self;
}

/* Wakes at least one thread waiting on this condition.
 */
static VALUE condition_signal(VALUE self)
{
    condition_t * cond = value_as_condition(self);
    chk_err(pthread_cond_signal, (&cond->shared->cond), "");
    return Qnil;
}

/* Wakes all threads waiting on this condition.
 */
static VALUE condition_broadcast(VALUE self)
{
    condition_t * cond = value_as_condition(self);
    chk_err(pthread_cond_broadcast, (&cond->shared->cond), "");
    return Qnil;
}

struct cond_wait_args {
    pthread_cond_t * cond;
    pthread_mutex_t * mu;
    struct timespec * tv;
};

static void cond_wait_cancel(struct cond_wait_args * args) {
    chk_err(pthread_cond_broadcast, (args->cond), "");
}

static int cond_wait_without_gvl(struct cond_wait_args * args) {
    if (args->tv) {
        return pthread_cond_timedwait(args->cond, args->mu, args->tv);
    } else {
        return pthread_cond_wait(args->cond, args->mu);
    }
}

/* @overload wait(mutex, timeout = nil)
 *   Releases the mutex lock and waits, reacquiring the mutex lock on wakeup.
 *   Returns after the specified timeout passes, if a timeout is specified.
 *   @param mutex [Mutex] the mutex
 *   @param timeout [Numeric] the timeout
 *   @return [Boolean] `false` if the wait timed out, `true` otherwise
 */
static VALUE condition_wait(int argc, VALUE * argv, VALUE self)
{
    VALUE mutex, timeout;
    rb_scan_args(argc, argv, "11", &mutex, &timeout);

    condition_t * cond = value_as_condition(self);
    mutex_t * mu = value_as_mutex(mutex);

    struct cond_wait_args args = { .cond = &cond->shared->cond, .mu = &mu->shared->mu };
    if (!NIL_P(timeout)) {
        struct timespec tv = {0};
        if (RB_TYPE_P(timeout, T_FIXNUM)) {
            tv.tv_sec = FIX2ULONG(timeout);
        } else if (RB_TYPE_P(timeout, T_BIGNUM)) {
            tv.tv_sec = rb_big2ull(timeout);
        } else if (RB_TYPE_P(timeout, T_FLOAT)) {
            double v = RFLOAT_VALUE(timeout) + 0.5e-9; // round up to nearest ns
            tv.tv_sec = (time_t)v;
            tv.tv_nsec = (v - tv.tv_sec) * 1000000000L;
        } else {
            rb_raise(rb_eArgError, "Invalid timeout");
        }
        args.tv = &tv;
    }

    int err = (int)(uintptr_t)rb_thread_call_without_gvl(
        (void * (*)(void *))cond_wait_without_gvl, &args,
        (void (*)(void *))cond_wait_cancel, &args);

    if (err == ETIMEDOUT) {
        return Qfalse;
    }

    rb_check_syserr_fail_strf(err, err, "pthread_cond_wait()");
    return Qtrue;
}

/* Calculate the number of shared memory space bytes required by instances of
 * the specified class. Recognized classes are {Mutex} and {Condition}.
 * @param type [Class] the specified class.
 * @raise [ArgumentError] if `type` is not a class or is an unrecognized class.
 */
static VALUE size_of(VALUE self, VALUE type)
{
    UNUSED(self);

    if (!RB_TYPE_P(type, T_CLASS)) {
        rb_raise(rb_eArgError, "Expected a class");
    }

    size_t s;
    if (type == Mutex) {
        s = sizeof(struct mutex_shared);
    } else if (type == Condition) {
        s = sizeof(struct condition_shared);
    } else {
        rb_raise(rb_eArgError, "Unrecognized type");
    }

    size_t o = s % 16;
    if (o) {
        s += 16-o;
    }
    return INT2FIX(s);
}

void Init_posix(VALUE ConcurrentSHM)
{
    rb_define_module_function(ConcurrentSHM, "size_of", size_of, 1);

    Finalizer = rb_define_class_under(ConcurrentSHM, "Finalizer", rb_cObject);
    rb_define_method(Finalizer, "call", __finalize, 1);

    SharedMemory = rb_define_class_under(ConcurrentSHM, "SharedMemory", rb_cIO);
    rb_define_module_function(SharedMemory, "open", shared_memory_open, -1);
    rb_define_module_function(SharedMemory, "delete", shared_memory_delete, 1);
    rb_define_method(SharedMemory, "initialize", shared_memory_initialize, 2);
    rb_define_method(SharedMemory, "size", shared_memory_size, 0);
    rb_define_method(SharedMemory, "size=", shared_memory_size_eq, 1);

    Region = rb_define_class_under(ConcurrentSHM, "Region", rb_cObject);
    rb_define_module_function(Region, "map", region_map, 3);
    rb_define_module_function(Region, "unmap", region_unmap, 1);
    rb_define_method(Region, "from", region_from, 0);
    rb_define_method(Region, "size", region_size, 0);
    rb_define_method(Region, "[]", region_at, -1);
    rb_define_method(Region, "alignment", region_alignment, 1);
    rb_define_method(Region, "aligned?", region_aligned_q, 1);
    rb_define_method(Region, "read", region_read, 0);
    rb_define_method(Region, "write", region_write, 1);
    rb_define_method(Region, "as_intptr", region_as_intptr, 0);
    rb_define_method(Region, "as_uintptr", region_as_uintptr, 0);

    Mutex = rb_define_class_under(ConcurrentSHM, "Mutex", rb_cObject);
    rb_define_alloc_func(Mutex, __mutex_alloc);
    rb_define_method(Mutex, "initialize", mutex_initialize, 1);
    rb_define_method(Mutex, "lock", mutex_lock, 0);
    rb_define_method(Mutex, "try_lock", mutex_try_lock, 0);
    rb_define_method(Mutex, "unlock", mutex_unlock, 0);

    Condition = rb_define_class_under(ConcurrentSHM, "Condition", rb_cObject);
    rb_define_alloc_func(Condition, __condition_alloc);
    rb_define_method(Condition, "initialize", condition_initialize, 1);
    rb_define_method(Condition, "signal", condition_signal, 0);
    rb_define_method(Condition, "broadcast", condition_broadcast, 0);
    rb_define_method(Condition, "wait", condition_wait, -1);
}
