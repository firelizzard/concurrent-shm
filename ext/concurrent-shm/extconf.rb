# frozen_string_literal: true

require 'mkmf'

fns = %w(shm_open shm_unlink fstat ftruncate mmap munmap)
fns += %w(pthread_mutexattr_init pthread_mutexattr_setpshared pthread_mutex_init pthread_mutexattr_destroy pthread_mutex_destroy)
fns += %w(pthread_condattr_init pthread_condattr_setpshared pthread_cond_init pthread_condattr_destroy pthread_cond_destroy)
fns += %w(pthread_mutex_lock pthread_mutex_trylock pthread_mutex_unlock pthread_cond_signal pthread_cond_broadcast pthread_cond_wait pthread_cond_timedwait)
fns.each { |fn| abort "missing #{fn}" unless have_func fn }

warn = RbConfig::MAKEFILE_CONFIG['warnflags'].split(' ')
warn -= ['-Wdeclaration-after-statement', '-Wtype-limits'] # things I don't care about
warn += ['-Wno-type-limits']
warn -= ['-Wno-self-assign', '-Wno-parentheses-equality', '-Wno-constant-logical-operand', '-Wno-cast-function-type'] # "unrecognized command line option"
$warnflags = warn.join(' ')

RbConfig::MAKEFILE_CONFIG['CC'] = ENV['CC'] if ENV['CC']

RbConfig::MAKEFILE_CONFIG['CFLAGS'] << ' -gdwarf-2 -g3 -O0' if ENV['debug']

dir_config('ruby')
with_cflags(RbConfig::MAKEFILE_CONFIG['CFLAGS']) { create_makefile('concurrent-shm/cshm') }
