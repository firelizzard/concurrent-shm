#include "main.h"

VALUE IntDomainError;

static inline int64_t __get_int_value(VALUE v) {
    if (v == Qtrue) {
        return 1;
    } else if (v == Qfalse) {
        return 0;
    } else if (RB_TYPE_P(v, T_FIXNUM)) {
        return FIX2LONG(v);
    } else {
        rb_raise(rb_eArgError, "Value must be an integer or boolean");
    }
}

static inline uint64_t __value2int(VALUE v, uint8_t bits)
{
    if (bits > 64) {
        rb_raise(rb_eException, "Internal error - Invalid bit size");
    }

    int64_t s = __get_int_value(v);
    if (bits == 64) {
        return s;
    }

    if (s > (1 << (bits - 1)) - 1) {
        rb_raise(IntDomainError, "Value overflows %d-bit signed integer", bits);
    }
    if (s < -(1 << (bits - 1))) {
        rb_raise(IntDomainError, "Value underflows %d-bit signed integer", bits);
    }
    return s;
}

static inline uint64_t __value2uint(VALUE v, uint8_t bits)
{
    if (bits > 64) {
        rb_raise(rb_eException, "Internal error - Invalid bit size");
    }

    int64_t s = __get_int_value(v);
    if (s < 0) {
        rb_raise(IntDomainError, "Cannot assign negative value to unsigned integer");
    }

    uint64_t u = (uint64_t)s;
    if (bits == 64) {
        return u;
    }

    if (u > (uint64_t)((1 << bits) - 1)) {
        rb_raise(IntDomainError, "Value overflows %d-bit unsigned integer", bits);
    }
    return u;
}

#define X(rtype, ctype, conv, size, ...) \
    VALUE rtype##Ptr; \
    static inline size_t __##ctype##_size(const void * _) { return sizeof(ctype##_t); } \
    static const rb_data_type_t __##ctype##_rb_type = { \
        .wrap_struct_name = #ctype "_t", \
        .function = { \
            .dfree = RUBY_TYPED_NEVER_FREE, \
            .dsize = (size_t (*)(const void*))__##ctype##_size, \
        }, \
    }; \
    Ptr2ValueFn(rtype, ctype) { return rb_data_typed_object_wrap(rtype##Ptr, (void *)v, &__##ctype##_rb_type); } \
    Value2PtrFn(rtype, ctype) { return (ctype##_t *)rb_check_typeddata(v, &__##ctype##_rb_type); } \
    static VALUE rtype##Ptr_read(VALUE self) { return INT2FIX(*Value2##rtype##Ptr(self)); } \
    static VALUE rtype##Ptr_write(VALUE self, VALUE v) { *Value2##rtype##Ptr(self) = (ctype##_t)conv(v, size); return self; }
PtrTypes
#undef X


/*
* Document-class: ConcurrentSHM::Value::IntDomainError
* Raised when writing an integer pointer under or overflows.
*/

/*
* Document-class: ConcurrentSHM::Value::Int8Ptr < ConcurrentSHM::Value::IntPtr
* A pointer to an 8-bit signed integer.
* @!method read
*   (see IntPtr#read)
* @!method write(value)
*   (see IntPtr#write)
*   @raise [ArgumentError] if the value is not an integer or boolean
*   @raise [IntDomainError] if the value is greater than 2<sup>7</sup>-1 or less than -2<sup>7</sup>
*/

/*
* Document-class: ConcurrentSHM::Value::Int16Ptr < ConcurrentSHM::Value::IntPtr
* A pointer to an 16-bit signed integer.
* @!method read
*   (see IntPtr#read)
* @!method write(value)
*   (see IntPtr#write)
*   @raise [ArgumentError] if the value is not an integer or boolean
*   @raise [IntDomainError] if the value is greater than 2<sup>15</sup>-1 or less than -2<sup>15</sup>
*/

/*
* Document-class: ConcurrentSHM::Value::Int32Ptr < ConcurrentSHM::Value::IntPtr
* A pointer to an 32-bit signed integer.
* @!method read
*   (see IntPtr#read)
* @!method write(value)
*   (see IntPtr#write)
*   @raise [ArgumentError] if the value is not an integer or boolean
*   @raise [IntDomainError] if the value is greater than 2<sup>31</sup>-1 or less than -2<sup>31</sup>
*/

/*
* Document-class: ConcurrentSHM::Value::Int64Ptr < ConcurrentSHM::Value::IntPtr
* A pointer to an 64-bit signed integer.
* @!method read
*   (see IntPtr#read)
* @!method write(value)
*   (see IntPtr#write)
*   @raise [ArgumentError] if the value is not an integer or boolean
*   @raise [IntDomainError] if the value is greater than 2<sup>63</sup>-1 or less than -2<sup>63</sup>
*/

/*
* Document-class: ConcurrentSHM::Value::UInt8Ptr < ConcurrentSHM::Value::IntPtr
* A pointer to an 8-bit unsigned integer.
* @!method read
*   (see IntPtr#read)
* @!method write(value)
*   (see IntPtr#write)
*   @raise [ArgumentError] if the value is not an integer or boolean
*   @raise [IntDomainError] if the value is greater than 2<sup>8</sup>-1 or negative
*/

/*
* Document-class: ConcurrentSHM::Value::UInt16Ptr < ConcurrentSHM::Value::IntPtr
* A pointer to an 16-bit unsigned integer.
* @!method read
*   (see IntPtr#read)
* @!method write(value)
*   (see IntPtr#write)
*   @raise [ArgumentError] if the value is not an integer or boolean
*   @raise [IntDomainError] if the value is greater than 2<sup>16</sup>-1 or negative
*/

/*
* Document-class: ConcurrentSHM::Value::UInt32Ptr < ConcurrentSHM::Value::IntPtr
* A pointer to an 32-bit unsigned integer.
* @!method read
*   (see IntPtr#read)
* @!method write(value)
*   (see IntPtr#write)
*   @raise [ArgumentError] if the value is not an integer or boolean
*   @raise [IntDomainError] if the value is greater than 2<sup>32</sup>-1 or negative
*/

/*
* Document-class: ConcurrentSHM::Value::UInt64Ptr < ConcurrentSHM::Value::IntPtr
* A pointer to an 64-bit unsigned integer.
* @!method read
*   (see IntPtr#read)
* @!method write(value)
*   (see IntPtr#write)
*   @raise [ArgumentError] if the value is not an integer or boolean
*   @raise [IntDomainError] if the value is greater than 2<sup>64</sup>-1 or negative
*/

void Init_PtrTypes(VALUE Value, VALUE IntPtr)
{
    IntDomainError = rb_define_class_under(Value, "IntDomainError", rb_eRangeError);

    #define X(rtype, ctype, ...) \
        rtype##Ptr = rb_define_class_under(Value, #rtype "Ptr", IntPtr); \
        rb_define_method(rtype##Ptr, "read", rtype##Ptr_read, 0); \
        rb_define_method(rtype##Ptr, "write", rtype##Ptr_write, 1);
    PtrTypes
    #undef X
}
