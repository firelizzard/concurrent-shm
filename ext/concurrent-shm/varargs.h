#define __STR(v) __STR1(v)
#define __STR1(v) __STR2(v)
#define __STR2(v) #v

#define __CAT(a, b) __CAT1(a, b)
#define __CAT1(a, b) __CAT2(a, b)
#define __CAT2(a, b) a##b

#define __NARG(...) __NARG_(__VA_ARGS__, __RSEQ_N())
#define __NARG_(...) __ARG_N(__VA_ARGS__)
#define __ARG_N(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, N, ...) N
#define __RSEQ_N() 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0

#define __KWARGS_DECL(n) \
    static ID kwarg_ids[1]; \
    VALUE opts, kwargs[1]; \
    if (!kwarg_ids[0])

#define __KWARGS_CALL(n, scan, required) \
    scan; \
    rb_get_kwargs(opts, kwarg_ids, required, n-required, kwargs);

#define __ARGS_0(fmt) rb_scan_args(argc, argv, fmt, &opts);
#define __ARGS_1(fmt, v0) rb_scan_args(argc, argv, fmt, &v0, &opts);
#define __ARGS_2(fmt, v0, v1) rb_scan_args(argc, argv, fmt, &v0, &v1, &opts);
#define __ARGS_3(fmt, v0, v1, v2) rb_scan_args(argc, argv, fmt, &v0, &v1, &v2, &opts);
#define __ARGS_4(fmt, v0, v1, v2, v3) rb_scan_args(argc, argv, fmt, &v0, &v1, &v2, &v3, &opts);
#define __ARGS_5(fmt, v0, v1, v2, v3, v4) rb_scan_args(argc, argv, fmt, &v0, &v1, &v2, &v3, &v4, &opts);
#define __ARGS_(N, fmt, ...) __CAT(__ARGS_, N)(fmt, __VA_ARGS__)
#define ARGS(fmt, ...) __ARGS_(__NARG(__VA_ARGS__), fmt, __VA_ARGS__)

#define __KWARGS_1(scan, required, v0) \
    __KWARGS_DECL(1) { CONST_ID(kwarg_ids[0], #v0); } \
    __KWARGS_CALL(1, scan, required); \
    VALUE v0 = kwargs[0];

#define __KWARGS_2(scan, required, v0, v1) \
    __KWARGS_DECL(2) { CONST_ID(kwarg_ids[0], #v0); CONST_ID(kwarg_ids[1], #v1); } \
    __KWARGS_CALL(2, scan, required); \
    VALUE v0 = kwargs[0]; VALUE v1 = kwargs[1];

#define __KWARGS_3(scan, required, v0, v1, v2) \
    __KWARGS_DECL(3) { CONST_ID(kwarg_ids[0], #v0); CONST_ID(kwarg_ids[1], #v1); CONST_ID(kwarg_ids[2], #v2); } \
    __KWARGS_CALL(3, scan, required); \
    VALUE v0 = kwargs[0]; VALUE v1 = kwargs[1]; VALUE v2 = kwargs[2];

#define __KWARGS_4(scan, required, v0, v1, v2, v3) \
    __KWARGS_DECL(4) { CONST_ID(kwarg_ids[0], #v0); CONST_ID(kwarg_ids[1], #v1); CONST_ID(kwarg_ids[2], #v2); CONST_ID(kwarg_ids[3], #v3); } \
    __KWARGS_CALL(4, scan, required); \
    VALUE v0 = kwargs[0]; VALUE v1 = kwargs[1]; VALUE v2 = kwargs[2]; VALUE v3 = kwargs[3];

#define __KWARGS_5(scan, required, v0, v1, v2, v3, v4) \
    __KWARGS_DECL(5) { CONST_ID(kwarg_ids[0], #v0); CONST_ID(kwarg_ids[1], #v1); CONST_ID(kwarg_ids[2], #v2); CONST_ID(kwarg_ids[3], #v3); CONST_ID(kwarg_ids[4], #v4); } \
    __KWARGS_CALL(5, scan, required); \
    VALUE v0 = kwargs[0]; VALUE v1 = kwargs[1]; VALUE v2 = kwargs[2]; VALUE v3 = kwargs[3]; VALUE v4 = kwargs[4];

#define __KWARGS_(N, scan, required, ...) __CAT(__KWARGS_, N)(scan, required, __VA_ARGS__)
#define KWARGS(scan, required, ...) __KWARGS_(__NARG(__VA_ARGS__), scan, required, __VA_ARGS__)

#define __CONST_IDS_1(pre, v0) static ID pre##v0; if (!pre##v0) { CONST_ID(pre##v0, #v0); }
#define __CONST_IDS_2(pre, v0, v1) static ID pre##v0, pre##v1; if (!pre##v0) { CONST_ID(pre##v0, #v0); CONST_ID(pre##v1, #v1); }
#define __CONST_IDS_3(pre, v0, v1, v2) static ID pre##v0, pre##v1, pre##v2; if (!pre##v0) { CONST_ID(pre##v0, #v0); CONST_ID(pre##v1, #v1); CONST_ID(pre##v2, #v2); }
#define __CONST_IDS_4(pre, v0, v1, v2, v3) static ID pre##v0, pre##v1, pre##v2, pre##v3; if (!pre##v0) { CONST_ID(pre##v0, #v0); CONST_ID(pre##v1, #v1); CONST_ID(pre##v2, #v2); CONST_ID(pre##v3, #v3); }
#define __CONST_IDS_5(pre, v0, v1, v2, v3, v4) static ID pre##v0, pre##v1, pre##v2, pre##v3, pre##v4; if (!pre##v0) { CONST_ID(pre##v0, #v0); CONST_ID(pre##v1, #v1); CONST_ID(pre##v2, #v2); CONST_ID(pre##v3, #v3); CONST_ID(pre##v4, #v4); }
#define __CONST_IDS_6(pre, v0, v1, v2, v3, v4, v5) static ID pre##v0, pre##v1, pre##v2, pre##v3, pre##v4, pre##v5; if (!pre##v0) { CONST_ID(pre##v0, #v0); CONST_ID(pre##v1, #v1); CONST_ID(pre##v2, #v2); CONST_ID(pre##v3, #v3); CONST_ID(pre##v4, #v4); CONST_ID(pre##v5, #v5); }
#define __CONST_IDS_7(pre, v0, v1, v2, v3, v4, v5, v6) static ID pre##v0, pre##v1, pre##v2, pre##v3, pre##v4, pre##v5, pre##v6; if (!pre##v0) { CONST_ID(pre##v0, #v0); CONST_ID(pre##v1, #v1); CONST_ID(pre##v2, #v2); CONST_ID(pre##v3, #v3); CONST_ID(pre##v4, #v4); CONST_ID(pre##v5, #v5); CONST_ID(pre##v6, #v6); }
#define __CONST_IDS_8(pre, v0, v1, v2, v3, v4, v5, v6, v7) static ID pre##v0, pre##v1, pre##v2, pre##v3, pre##v4, pre##v5, pre##v6, pre##v7; if (!pre##v0) { CONST_ID(pre##v0, #v0); CONST_ID(pre##v1, #v1); CONST_ID(pre##v2, #v2); CONST_ID(pre##v3, #v3); CONST_ID(pre##v4, #v4); CONST_ID(pre##v5, #v5); CONST_ID(pre##v6, #v6); CONST_ID(pre##v7, #v7); }
#define __CONST_IDS_9(pre, v0, v1, v2, v3, v4, v5, v6, v7, v8) static ID pre##v0, pre##v1, pre##v2, pre##v3, pre##v4, pre##v5, pre##v6, pre##v7, pre##v8; if (!pre##v0) { CONST_ID(pre##v0, #v0); CONST_ID(pre##v1, #v1); CONST_ID(pre##v2, #v2); CONST_ID(pre##v3, #v3); CONST_ID(pre##v4, #v4); CONST_ID(pre##v5, #v5); CONST_ID(pre##v6, #v6); CONST_ID(pre##v7, #v7); CONST_ID(pre##v8, #v8); }
#define __CONST_IDS_10(pre, v0, v1, v2, v3, v4, v5, v6, v7, v8, v9) static ID v0, pre##v1, pre##v2, pre##v3, pre##v4, pre##v5, pre##v6, pre##v7, pre##v8, pre##v9; if (!pre##v0) { CONST_ID(pre##v0, #v0); CONST_ID(pre##v1, #v1); CONST_ID(pre##v2, #v2); CONST_ID(pre##v3, #v3); CONST_ID(pre##v4, #v4); CONST_ID(pre##v5, #v5); CONST_ID(pre##v6, #v6); CONST_ID(pre##v7, #v7); CONST_ID(pre##v8, #v8); CONST_ID(pre##v9, #v9); }
#define __CONST_IDS_(N, pre, ...) __CAT(__CONST_IDS_, N)(pre, __VA_ARGS__)
#define CONST_IDS(pre, ...) __CONST_IDS_(__NARG(__VA_ARGS__), pre, __VA_ARGS__)
